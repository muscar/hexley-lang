#include <cassert>

#define require(T, M) assert((T) && (M))

#define ensure(T, M) assert((T) && (M))
