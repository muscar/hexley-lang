#include <vector>
#include <experimental/optional>
#include "dbc.hpp"

using namespace std::experimental;

namespace macho
{
    template<typename T>
    void stream_write(std::ostream &os, T n)
    {
        os.write(reinterpret_cast<const char *>(&n), sizeof(T));
    }

    template<typename T>
    constexpr T align(T value, uint32_t alignment)
    {
        return (value + (alignment - 1)) & ~(alignment - 1);
    }

    template<typename T>
    constexpr T operator|(T x, T y)
    {
        using enum_type = typename std::underlying_type<T>::type;
        return static_cast<T>(static_cast<enum_type>(x) | static_cast<enum_type>(y));
    }

     const uint32_t magic_64 = 0xfeedfacf;

    enum class cpu_type_t : uint32_t
    {
        x86_64 = 0x1000007
    };

    enum class cpu_subtype_t : uint32_t
    {
        x86_64_all = 0x03,
        lib64 = 0x80000000
    };

    enum class file_type_t : uint32_t
    {
        object = 0x01,
        execute = 0x02,
        core = 0x04,
        preload = 0x05,
        dylib = 0x06,
        dylinker = 0x07,
        bundle = 0x08,
        dsym = 0x0A
    };

    enum class flags_t : uint32_t
    {
        noundefs = 0x01,
        incrlink = 0x02,
        dyldlink = 0x04,
        twolevel = 0x80,
        bindatload = 0x08,
        prebound = 0x10,
        prebindable = 0x800,
        nofixprebinding = 0x400,
        allmodsbound = 0x1000,
        canonical = 0x4000,
        split_segs = 0x20,
        force_flat = 0x100,
        subsections_via_symbols = 0x2000,
        nomultidefs = 0x200,
        pie = 0x200000
    };

    enum class load_command_t : uint32_t
    {
        symtab = 0x02,
        segment_64 = 0x19,
        version_min_macosx = 0x24
    };

    enum class vm_prot_t : uint32_t
    {
        none = 0x00,
        read = 0x01,
        write = 0x02,
        execute = 0x04
    };

    enum class section_type_t : uint32_t
    {
        regular = 0x00,
        zerofill = 0x01,
        cstring_literals = 0x02,
        four_byte_literals = 0x03,
        eight_byte_literals = 0x04,
        literal_pointers = 0x05,
        non_lazy_symbol_pointers = 0x06,
        lazy_symbol_pointers = 0x07,
        symbol_stubs = 0x08,
        mod_init_func_pointers = 0x09,
        mod_term_func_pointers = 0x0a,
        coalesced = 0x0b,
        gb_zerofill = 0x0c,
    };

    enum class section_attr_t : uint32_t
    {
        pure_instructions = 1U << 31,
        no_toc = 1U << 30,
        strip_static_syms = 1U << 29,
        no_dead_strip = 1U << 28,
        live_support = 1U << 27,
        self_modifying_code = 1U << 26,
        debug = 1U << 25,
        some_instructions = 1U << 10,
        ext_reloc = 1U << 9,
        loc_reloc = 1U << 8 
    };

    enum class symbol_flags_t : uint8_t
    {
        stab = 0xe0,
        pext = 0x10,
        type = 0x0e,
        ext = 0x01,
        // type
        undf = 0x00,
        abs = 0x02,
        sect = 0x0e,
        pbud = 0x0c,
        indr = 0x0a
    };

    enum class symbol_desc_t : uint16_t
    {
        reference_flag_undefined_non_lazy = 0x00,
        reference_flag_undefined_lazy = 0x01,
        reference_flag_defined = 0x02,
        reference_flag_private_defined = 0x03,
        reference_flag_private_undefined_non_lazy = 0x04,
        reference_flag_private_undefined_lazy = 0x05,
        referenced_dynamically = 0x10,
        desc_discarded = 0x20,
        weak_ref = 0x40,
        weak_def = 0x80
    };

    const uint8_t no_sect = 0;

   struct mach_header_64
    {
        uint32_t magic;
        cpu_type_t cputype;
        cpu_subtype_t cpusubtype;
        file_type_t filetype;
        uint32_t ncmds = 0;
        uint32_t sizeofcmds = 0;
        flags_t flags;
        uint32_t reserved = 0;

        mach_header_64(uint32_t magic, cpu_type_t cputype, cpu_subtype_t cpusubtype, file_type_t filetype, flags_t flags)
        : magic{magic}, cputype{cputype}, cpusubtype{cpusubtype}, filetype{filetype}, flags{flags}
        { }

        void dump(std::ostream &os) const
        {
            stream_write(os, magic);
            stream_write(os, static_cast<uint32_t>(cputype));
            stream_write(os, static_cast<uint32_t>(cpusubtype));
            stream_write(os, static_cast<uint32_t>(filetype));
            stream_write(os, ncmds);
            stream_write(os, sizeofcmds);
            stream_write(os, static_cast<uint32_t>(flags));
            stream_write(os, reserved);
        }
    };

    struct section_64
    {
        char sectname[16] = { };
        char segname[16] = { };
        uint64_t addr;
        uint64_t size;
        uint32_t offset = 0;
        uint32_t align;
        uint32_t reloff;
        uint32_t nreloc;
        uint32_t flags;
        uint32_t reserved1 = 0;
        uint32_t reserved2 = 0;
        uint32_t reserved3 = 0;

        section_64(std::string segname_, std::string name, uint64_t addr, uint64_t size, uint32_t align, uint32_t reloff, uint32_t nreloc, section_type_t type, section_attr_t attr)
        : addr{addr}, size{size}, align{align}, reloff{reloff}, nreloc{nreloc}, flags{static_cast<uint32_t>(type) | static_cast<uint32_t>(attr)}
        {
            require(segname_.size() < 16, "segment name longer than 16 chars");
            require(name.size() < 16, "section name longer than 16 chars");
            strncpy(segname, segname_.c_str(), segname_.size());
            strncpy(sectname, name.c_str(), name.size());
        }

        void dump(std::ostream &os) const
        {
            os.write(sectname, 16);
            os.write(segname, 16);
            stream_write(os, addr);
            stream_write(os, size);
            stream_write(os, offset);
            stream_write(os, align);
            stream_write(os, reloff);
            stream_write(os, nreloc);
            stream_write(os, flags);
            stream_write(os, reserved1);
            stream_write(os, reserved2);
            stream_write(os, reserved3);
        }
    };

    struct nlist_64
    {
        union { uint32_t n_strx; } n_un;
        symbol_flags_t n_type;
        uint8_t n_sect;
        symbol_desc_t n_desc;
        uint64_t n_value;

        nlist_64(uint32_t idx, symbol_flags_t type, uint8_t sec, symbol_desc_t desc, uint64_t value)
        : n_type{type}, n_sect{sec}, n_desc{desc}, n_value{value}
        {
            n_un.n_strx = idx;
        }

        void dump(std::ostream &os) const
        {
            stream_write(os, n_un.n_strx);
            os.put(static_cast<uint8_t>(n_type));
            os.put(n_sect);
            stream_write(os, static_cast<uint16_t>(n_desc));
            stream_write(os, n_value);
        }
    };

    struct load_command
    {
        load_command_t cmd;
        uint32_t cmdsize;

        load_command(load_command_t cmd, uint32_t cmdsize) : cmd{cmd}, cmdsize{cmdsize} { }

        virtual void dump(std::ostream &os) const = 0;
    };

    struct segment_command_64 : load_command
    {
        char segname[16] = { };
        uint64_t vmaddr;
        optional<uint64_t> vmsize;
        uint64_t fileoff = 0;
        uint64_t filesize;
        vm_prot_t maxprot;
        vm_prot_t initprot;
        uint32_t nsects = 0;
        uint32_t flags;

        segment_command_64(std::string name, uint64_t vmaddr, optional<uint64_t> vmsize, uint64_t filesize, vm_prot_t maxprot, vm_prot_t initprot, uint32_t flags)
        : load_command{load_command_t::segment_64, 72},
          vmaddr{vmaddr}, vmsize{vmsize}, filesize{filesize}, maxprot{maxprot}, initprot{initprot}, flags{flags}
        {
            require(name.size() < 16, "segment name longer than 16 chars");
            strncpy(segname, name.c_str(), name.size());
        }

        void dump(std::ostream &os) const override
        {
            stream_write(os, static_cast<uint32_t>(cmd));
            stream_write(os, cmdsize);
            os.write(segname, 16);
            stream_write(os, vmaddr);
            stream_write(os, vmsize ? *vmsize : filesize);
            stream_write(os, fileoff);
            stream_write(os, filesize);
            stream_write(os, static_cast<uint32_t>(maxprot));
            stream_write(os, static_cast<uint32_t>(initprot));
            stream_write(os, nsects);
            stream_write(os, flags);
        }
    };

    struct version_min_macosx : load_command
    {
        uint32_t version;
        uint32_t reserved = 0;

        version_min_macosx(uint32_t version)
        : load_command{load_command_t::version_min_macosx, 16}, version{version}
        { }

        void dump(std::ostream &os) const override
        {
            stream_write(os, static_cast<uint32_t>(cmd));
            stream_write(os, cmdsize);
            stream_write(os, version);
            stream_write(os, reserved);
        }
    };

    struct symtab_command : load_command
    {
        uint32_t symoff;
        uint32_t nsyms = 0;
        uint32_t stroff = 0;
        uint32_t strsize = 0;

        symtab_command()
        : load_command{load_command_t::symtab, 24}
        { }

        void dump(std::ostream &os) const override
        {
            stream_write(os, static_cast<uint32_t>(cmd));
            stream_write(os, cmdsize);
            stream_write(os, symoff);
            stream_write(os, nsyms);
            stream_write(os, stroff);
            stream_write(os, strsize);
        }
    };

    struct mach_64
    {
        using segment_handle = std::vector<std::unique_ptr<load_command>>::size_type;
        using section_handle = std::vector<section_64>::size_type;

        mach_header_64 header;

        uint64_t fileoff = sizeof(mach_header_64);

        std::vector<segment_command_64> segment_commands;
        std::vector<int16_t> section_map;
        std::vector<std::unique_ptr<load_command>> load_commands;
        std::vector<section_64> sections;
        std::vector<std::tuple<const uint8_t *, uint64_t>> blobs;
        symtab_command symtab{};
        std::vector<nlist_64> name_list;
        std::vector<std::string> string_table;

        mach_64(uint32_t magic, cpu_type_t cputype, cpu_subtype_t cpusubtype, file_type_t filetype, flags_t flags)
        : header{magic, cputype, cpusubtype, filetype, flags}
        { }

        void register_load_command(load_command &cmd)
        {
            header.ncmds++;
            header.sizeofcmds += cmd.cmdsize;
            fileoff += cmd.cmdsize;
        }

        void init_symtab(uint16_t sym_cnt)
        {
            symtab.strsize = 0;
            symtab.symoff = fileoff;
            symtab.stroff = fileoff + 16 * sym_cnt;
        }

        void set_min_osx_version(uint16_t major, uint16_t minor)
        {
            uint32_t min_osx_version = major << 16 | minor << 8;
            auto cmd = std::make_unique<version_min_macosx>(min_osx_version);
            register_load_command(*cmd);
            load_commands.emplace_back(std::move(cmd));
        }

        segment_handle add_segment(std::string name, uint64_t vmaddr, optional<uint64_t> vmsize, vm_prot_t maxprot, vm_prot_t initprot, uint32_t flags = 0)
        {
            auto seg = segment_commands.size();
            auto cmd = segment_command_64{name, vmaddr, vmsize, 0, maxprot, initprot, flags};
            segment_commands.emplace_back(cmd);
            register_load_command(cmd);
            section_map.push_back(-1);
            return seg;
        }

        section_handle add_section(segment_handle h, std::string name, uint64_t addr, uint64_t size, uint32_t align, section_type_t type, section_attr_t attr)
        {
            require(h < segment_commands.size(), "invalid segment handle");
            auto cmd = &segment_commands[h];
            cmd->nsects++;
            cmd->cmdsize += 80;
            cmd->filesize += size;
            header.sizeofcmds += 80;
            fileoff += 80;
            auto sec = sections.size();
            sections.emplace_back(cmd->segname, name, addr, size, align, 0, 0, type, attr);
            if (section_map[h] == -1)
                section_map[h] = sec;
            return sec;
        }

        void add_section_data(section_handle h, const uint8_t *blob, uint64_t len)
        {
            require(h < sections.size(), "invalid section handle");
            auto sec = &sections[h];
            sec->offset = fileoff;
            fileoff += align(len, 8);
            blobs.push_back(std::make_tuple(blob, len));
        }

        void add_symbol(std::string name, section_handle sec, uint64_t value)
        {
            symtab.nsyms++;
            string_table.push_back(name);
            auto idx = symtab.strsize;
            symtab.strsize += name.size() + 1;
            name_list.emplace_back(idx + 1, symbol_flags_t::ext | symbol_flags_t::sect, sec + 1, symbol_desc_t::reference_flag_defined, value);
            fileoff += 16;
        }

        void dump(std::ostream &os) const
        {
            header.dump(os);
            for (size_t i = 0; i < segment_commands.size(); ++i) {
                segment_commands[i].dump(os);
                auto first_sec = section_map[i];
                auto last_sec = (i + 1) < section_map.size() ? section_map[i + 1] : section_map.size();
                require(first_sec != -1, "empty segment");
                for (size_t j = first_sec; j < last_sec; ++j)
                    sections[j].dump(os);
            }
            for (auto &&cmd : load_commands)
                cmd->dump(os);
            symtab.dump(os);
            for (auto &&blob : blobs) {
                auto data = std::get<0>(blob);
                auto len = std::get<1>(blob);
                os.write(reinterpret_cast<const char *>(data), len);
                auto sz = align(len, 8);
                for (size_t i = 0; i < sz - len; ++i)
                    os.put(0);
            }
            for (auto &&name : name_list)
                name.dump(os);
            for (auto &&str : string_table) {
                os.put(0);
                os.write(str.data(), str.size());
            }
            auto sz = align(symtab.strsize, 8);
            for (size_t i = 0; i < sz - symtab.strsize; ++i)
                os.put(0);
        }
    };
}

