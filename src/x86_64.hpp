#pragma once

#include <vector>
#include <map>

#include "dbc.hpp"

namespace x86_64
{
    enum class reg : uint8_t
    {
        rax = 0,
        rcx,
        rdx,
        rbx,
        rsp,
        rbp,
        rsi,
        rdi,
        r8,
        r9,
        r10,
        r11,
        r12,
        r13,
        r14,
        r15
    };

    struct address
    {
        reg base;
        size_t offset;
    };

    bool is_reg64(reg r)
    {
        return static_cast<uint8_t>(r) > 7;
    }

    uint8_t modrm(uint8_t mod, uint8_t reg, uint8_t rm)
    {
        return ((mod & 0x03) << 6) | ((reg & 0x07) << 3) | (rm & 0x07);
    }

    uint8_t rex(uint8_t w, uint8_t r, uint8_t x, uint8_t b)
    {
        return 0x40 | ((w & 0x01) << 3) | ((r & 0x01) << 2) | ((x & 0x01) << 1) | (b & 0x01);
    }

    class assembler
    {
        std::vector<uint8_t> buf;

    public:
        const uint8_t *data() const
        {
            return buf.data();
        }

        size_t size() const
        {
            return buf.size();
        }

        void emit_uint8(uint8_t b)
        {
            buf.emplace_back(b);
        }

        void emit_int32(int32_t i)
        {
            emit_uint8(i & 0xff);
            emit_uint8((i >> 8) & 0xff);
            emit_uint8((i >> 16) & 0xff);
            emit_uint8((i >> 24) & 0xff);
        }

        void emit_int64(int64_t i)
        {
            emit_int32(i & 0xffffffff);
            emit_int32((i >> 32) & 0xffffffff);
        }

        void add(const reg r1, const reg r2)
        {
            // REX.W + 03 /r
            emit_uint8(rex(1, is_reg64(r2), 0, is_reg64(r1)));
            emit_uint8(0x03);
            emit_uint8(modrm(0x03, static_cast<uint8_t>(r1), static_cast<uint8_t>(r2)));
        }

        void add(const reg r, int32_t imm)
        {
            // REX.W + 81 /0 id
            emit_uint8(rex(1, 0, 0, is_reg64(r)));
            emit_uint8(0x81);
            emit_uint8(modrm(0x03, 0, static_cast<uint8_t>(r)));
            emit_int32(imm);
        }

        void mov(const reg r, int64_t imm)
        {
            // REX.W + B8 + rd io
            emit_uint8(rex(1, 0, 0, is_reg64(r)));
            emit_uint8(0xb8 + (static_cast<uint8_t>(r) % 8));
            emit_int64(imm);
        }

        void mov(const reg r1, const reg r2)
        {
            // REX.W + 89 /r
            emit_uint8(rex(1, is_reg64(r2), 0, is_reg64(r1)));
            emit_uint8(0x89);
            emit_uint8(modrm(0x03, static_cast<uint8_t>(r2), static_cast<uint8_t>(r1)));
        }

        void mov(address addr, const reg r)
        {
            require(addr.base != reg::rsp, "not implemented");
            // REX.W + 89 /r
            emit_uint8(rex(1, is_reg64(r), 0, 0));
            emit_uint8(0x89);
            emit_uint8(modrm(0x02, static_cast<uint8_t>(r), static_cast<uint8_t>(addr.base)));
            emit_int32(addr.offset);
        }

        void mov(const reg r, const address &addr)
        {
            require(addr.base != reg::rsp, "not implemented");
            // REX.W + 8B /r
            emit_uint8(rex(1, is_reg64(r), 0, 0));
            emit_uint8(0x8b);
            emit_uint8(modrm(0x02, static_cast<uint8_t>(r), static_cast<uint8_t>(addr.base)));
            emit_int32(addr.offset);
        }

        void pop(const reg r)
        {
            // 58 + rd
            emit_uint8(0x58 + static_cast<uint8_t>(r));
        }

        void push(const reg r)
        {
            // 50 + rd
            emit_uint8(0x50 + static_cast<uint8_t>(r));
        }

        void ret()
        {
            // C3
            emit_uint8(0xc3);
        }
    };

    class stack_frame
    {
        size_t offset = -8;

        const std::vector<reg> param_regs_ = {reg::rdi, reg::rsi, reg::rdx, reg::rcx, reg::r8, reg::r9};
    public:
        reg result_reg() const
        {
            return reg::rax;
        }

        reg base_reg() const
        {
            return reg::rbp;
        }

        reg param_reg(uint8_t param_idx) const
        {
            require(param_idx < 6, "functions limited to 6 integer sized params at the moment");
            return param_regs_[param_idx];
        }

        const std::vector<reg> &param_regs() const
        {
            return param_regs_;
        }

        size_t declare_local(size_t size)
        {
            auto off = offset;
            offset -= size;
            return off;
        }

        void prologue(assembler &asm64)
        {
            asm64.push(reg::rbp);
            asm64.mov(reg::rbp, reg::rsp);
        }

        void epilogue(assembler &asm64)
        {
            asm64.pop(reg::rbp);
            asm64.ret();
        }

        std::vector<reg> free_regs() const
        {
            return {reg::rax, reg::r10, reg::r11};
        }
    };
}

