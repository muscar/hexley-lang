#include <iostream>
#include <fstream>

#include "parser.hpp"
#include "codegen.hpp"

std::string red(std::string text)
{
    return "\033[1;31m" + text + "\033[0m";
}

void report_error(std::string msg)
{
    std::cerr << red("Error: ") << msg << std::endl;
}

void banner()
{
    std::cout << "Hexley compiler v0.1 (c) Alex Muscar 2015" << std::endl;
}

std::string basename(std::string pathname)
{
    return std::string(std::find(pathname.rbegin(), pathname.rend(), '/').base(), end(pathname));
}

std::string remove_extension(std::string filename)
{
    auto pivot = std::find(filename.rbegin(), filename.rend(), '.');
    return pivot == filename.rend() ? filename : std::string(begin(filename), pivot.base() - 1);
}

int main(int argc, char *argv[])
{
    if (argc != 2) {
        std::cerr << "usage: " << argv[0] << " <path>" << std::endl;
        return 1;
    }

    banner();

    auto module_name = remove_extension(basename(argv[1]));
    std::ifstream in{argv[1]};
    obc::backend::compiler_ctx ctx{report_error};
    ctx.symtab.enter();
    obc::frontend::parse(ctx, in);
    obc::backend::write_object_file(ctx, module_name);
    ctx.symtab.exit();

    return 0;
}

