#pragma once

#include <map>
#include <vector>

#include <cstdint>

struct type
{
    std::string name;
    size_t size;
};

std::map<std::string, type> builtin_types = {
    {"i32", {"i32", 8}}
};

const type &resolve_type(std::string name)
{
    auto it = builtin_types.find(name);
    if (it == std::end(builtin_types))
        throw std::runtime_error{"unknown type: " + name};
    return it->second;
}

enum class symbol_kind
{
    param,
    local,
    global,
    fun
};

struct symbol
{
    symbol_kind kind;
    type type;
    int64_t data;
};

class symtab
{
    std::vector<std::map<std::string, symbol>> scopes;
public:
    void enter()
    {
        scopes.emplace_back();
    }

    void exit()
    {
        scopes.pop_back();
    }

    void add(std::string name, symbol sym)
    {
        scopes.back()[name] = sym;
    }

    optional<symbol> find(std::string name) const
    {
        for (auto it = std::rbegin(scopes); it != std::rend(scopes); ++it) {
            auto sit = it->find(name);
            if (sit != std::end(*it))
                return sit->second;
        }
        return nullopt;
    }

    const auto &current_scope() const
    {
        return scopes.back();
    }

    size_t size() const
    {
        return scopes.size();
    }
};

