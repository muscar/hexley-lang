#pragma once

#include <istream>
#include <map>

#include <experimental/optional>

using namespace std::experimental;

namespace obc::frontend
{
    enum class token_kind
    {
        id,
        int_lit,
        kw_fun,
        kw_end,
        kw_return,
        kw_var,
        plus,
        eq,
        lpar,
        rpar,
        comma,
        colon,
        semicolon,
        unknown,
        eof
    };

    const char *token_names[] = {
        "identifier",
        "integer literal",
        "fun keyword",
        "end keyword",
        "return keyword",
        "var keyword",
        "plus",
        "equals",
        "left parenthesis",
        "right parenthesis",
        "comma",
        "colon",
        "semicolon",
        "unknown token",
        "end of file",
    };

    std::string to_string(token_kind kind)
    {
        return token_names[static_cast<uint8_t>(kind)];
    }

    struct token
    {
        size_t line, col;
        token_kind kind;
        std::string text;
    };

    template<typename Pred>
    std::string read_while(std::istream &in, Pred pred)
    {
        std::string acc;
        while (!in.eof() && pred(in.peek()))
            acc.push_back(in.get());
        return acc;
    }

    class lexer
    {
        static std::map<std::string, token_kind> keywords;

        std::istream &in;
        size_t line = 0, col = 0;
        optional<token> lookahead = nullopt;
    public:
        lexer(std::istream &in) : in{in} { }

        token peek()
        {
            if (!lookahead)
                lookahead = next();
            return *lookahead;
        }

        token next()
        {
            if (lookahead) {
                auto tok = *lookahead;
                lookahead = nullopt;
                return tok;
            }
            while (!in.eof() && isspace(in.peek()))
                in.get();
            if (in.eof())
                return {line, col, token_kind::eof, "end of file"};
            switch (in.peek()) {
                case '+':
                    in.get();
                    return {line, col, token_kind::plus, "+"};
                case '=':
                    in.get();
                    return {line, col, token_kind::eq, "="};
                case ',':
                    in.get();
                    return {line, col, token_kind::comma, ","};
                case ':':
                    in.get();
                    return {line, col, token_kind::colon, ":"};
                case ';':
                    in.get();
                    return {line, col, token_kind::semicolon, ";"};
                case '(':
                    in.get();
                    return {line, col, token_kind::lpar, "("};
                case ')':
                    in.get();
                    return {line, col, token_kind::rpar, ")"};
            }
            if (isdigit(in.peek()))
                return num_lit();
            if (isalnum(in.peek()))
                return kw_or_id();
            return unknown();
        }

        token num_lit()
        {
            return {line, col, token_kind::int_lit, read_while(in, isdigit)};
        }

        token kw_or_id()
        {
            auto lexeme = read_while(in, isalnum);
            auto it = keywords.find(lexeme);
            if (it != std::end(keywords))
                return {line, col, it->second, lexeme};
            return {line, col, token_kind::id, lexeme};
        }

        token unknown()
        {
            return {line, col, token_kind::unknown, read_while(in, [](char c) {
                return !isspace(c);
            })};
        }
    };

    std::map<std::string, token_kind> lexer::keywords = {
        {"fun", token_kind::kw_fun},
        {"end", token_kind::kw_end},
        {"return", token_kind::kw_return},
        {"var", token_kind::kw_var},
    };
};

