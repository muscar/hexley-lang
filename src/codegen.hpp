#pragma once

#include <algorithm>
#include <vector>

#include "x86_64.hpp"
#include "semantic.hpp"
#include "macho.hpp"
#include "dbc.hpp"

using namespace macho;

namespace obc::backend
{
    class reg_alloc
    {
        std::vector<x86_64::reg> regs;
    public:
        void put(x86_64::reg r)
        {
            require(std::find(std::begin(regs), std::end(regs), r) == std::end(regs),
                    "register is already available");
            regs.push_back(r);
        }

        optional<x86_64::reg> get()
        {
            if (regs.empty())
                return nullopt;
            auto r = regs.back();
            regs.pop_back();
            return r;
        }
    };

    struct compiler_ctx
    {
        void (*error_handler)(std::string);
        x86_64::assembler asm64;
        reg_alloc regs;
        x86_64::stack_frame frame;
        symtab symtab;

        compiler_ctx(void (*error_handler)(std::string))
        : error_handler{error_handler}
        {
            for (auto r : frame.free_regs())
                regs.put(r);
        }
    };

    enum class operand_kind
    {
        reg,
        addr,
        imm
    };

    struct operand
    {
        operand_kind kind;
        union
        {
            x86_64::reg reg;
            x86_64::address addr;
            int64_t imm;
        };

        operand(x86_64::reg reg) : kind{operand_kind::reg}, reg{reg} { }

        operand(x86_64::address addr) : kind{operand_kind::addr}, addr{addr} { }

        operand(int64_t imm) : kind{operand_kind::imm}, imm{imm} { }
    };

    x86_64::reg load(compiler_ctx &ctx, const operand &op)
    {
        switch (op.kind) {
            case operand_kind::imm: {
                auto r = ctx.regs.get();
                if (!r)
                    throw std::runtime_error{"out of regs"};
                ctx.asm64.mov(*r, op.imm);
                return *r;
            }
            case operand_kind::addr: {
                auto r = ctx.regs.get();
                if (!r)
                    throw std::runtime_error{"out of regs"};
                ctx.asm64.mov(*r, op.addr);
                return *r;
            }
            case operand_kind::reg: {
                auto r = ctx.regs.get();
                if (!r)
                    throw std::runtime_error{"out of regs"};
                ctx.asm64.mov(*r, op.reg);
                return *r;
            }
        }
    }

    void write_object_file(compiler_ctx &ctx, std::string module_name)
    {
        require(ctx.symtab.size() == 1, "the symbol table has more than one active scope");

        mach_64 m{magic_64, cpu_type_t::x86_64, cpu_subtype_t::x86_64_all,
                  file_type_t::object, flags_t::subsections_via_symbols};
        m.set_min_osx_version(10, 8);

        auto vm_prot = vm_prot_t::read | vm_prot_t::write | vm_prot_t::execute;
        auto text_seg = m.add_segment("__TEXT", 0, nullopt, vm_prot, vm_prot);
        auto text_sec = m.add_section(text_seg, "__text", 0, ctx.asm64.size(), 4, section_type_t::regular, section_attr_t::pure_instructions | section_attr_t::some_instructions);

        m.register_load_command(m.symtab);
        m.add_section_data(text_sec, ctx.asm64.data(), ctx.asm64.size());

        m.init_symtab(ctx.symtab.current_scope().size());
        for (auto &&it : ctx.symtab.current_scope())
            if (it.second.kind == symbol_kind::fun)
                m.add_symbol("_" + it.first, text_sec, it.second.data);

        std::fstream obj_file{module_name + ".o", std::ios::out | std::ios::binary};
        m.dump(obj_file);
    }
}

