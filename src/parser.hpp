#pragma once

#include <algorithm>
#include <vector>

#include <cstdint>

#include <experimental/optional>

#include "lexer.hpp"
#include "codegen.hpp"
#include "dbc.hpp"

using namespace std::experimental;
using namespace obc::backend;

namespace obc::frontend
{
    optional<token> expect(compiler_ctx &ctx, lexer &lex, token_kind expected)
    {
        auto tok = lex.next();
        if (tok.kind != expected) {
            ctx.error_handler("expecting " + to_string(expected) + ", but got " + tok.text);
            return nullopt;
        }
        return tok;
    }

    type parse_type_annotation(compiler_ctx &ctx, lexer &lex)
    {
        expect(ctx, lex, token_kind::colon);
        auto ty_annot = expect(ctx, lex, token_kind::id);
        return resolve_type(ty_annot->text);
    }

    operand parse_factor(compiler_ctx &ctx, lexer &lex)
    {
        auto tok = lex.next();
        switch (tok.kind) {
            case token_kind::int_lit:
                return {std::stoll(tok.text)};
            case token_kind::id: {
                auto sym = ctx.symtab.find(tok.text);
                if (!sym)
                    throw std::runtime_error{"unbound symbol: " + tok.text};
                switch (sym->kind) {
                    case symbol_kind::param:
                        return {ctx.frame.param_reg(sym->data)};
                    case symbol_kind::local:
                        return {{ctx.frame.base_reg(), static_cast<size_t>(sym->data)}};
                    default:
                        throw std::runtime_error{"not implemented"};
                }
            }
            default:
                ctx.error_handler("expecting expression, but got " + tok.text);
                assert(false);
        }
    }

    operand parse_exp(compiler_ctx &ctx, lexer &lex)
    {
        auto r1 = load(ctx, parse_factor(ctx, lex));
        while (lex.peek().kind == token_kind::plus) {
            lex.next();
            auto op2 = parse_factor(ctx, lex);
            if (op2.kind == operand_kind::imm) {
                ctx.error_handler("implicitly converting 64 bit immediate to 32 bit immediate (will fix later)");
                ctx.asm64.add(r1, static_cast<int32_t>(op2.imm));
            } else {
                auto r2 = load(ctx, op2);
                ctx.asm64.add(r1, r2);
                ctx.regs.put(r2);
            }
        }
        return {r1};
    }

    void parse_stmt(compiler_ctx &ctx, lexer &lex)
    {
        auto tok = lex.next();
        switch (tok.kind) {
            case token_kind::kw_var: {
                auto name = expect(ctx, lex, token_kind::id);
                auto ty = parse_type_annotation(ctx, lex);
                auto off = ctx.frame.declare_local(ty.size);
                ctx.symtab.add(name->text, {symbol_kind::local, ty, static_cast<int64_t>(off)});
                expect(ctx, lex, token_kind::eq);
                auto reg = load(ctx, parse_exp(ctx, lex));
                ctx.asm64.mov({ctx.frame.base_reg(), off}, reg);
                ctx.regs.put(reg);
            }
                break;
            case token_kind::kw_return: {
                auto reg = load(ctx, parse_exp(ctx, lex));
                if (reg != ctx.frame.result_reg())
                    ctx.asm64.mov(ctx.frame.result_reg(), reg);
                ctx.frame.epilogue(ctx.asm64);
            }
                break;
            default:
                ctx.error_handler("expecting statement, but got " + tok.text);
                break;
        }
    }

    void parse_function_def(compiler_ctx &ctx, lexer &lex)
    {
        expect(ctx, lex, token_kind::kw_fun);
        auto name = expect(ctx, lex, token_kind::id);

        ctx.symtab.enter();

        expect(ctx, lex, token_kind::lpar);
        if (lex.peek().kind != token_kind::rpar) {
            auto param_idx = 0;
            auto param = expect(ctx, lex, token_kind::id);
            auto ty = parse_type_annotation(ctx, lex);
            ctx.symtab.add(param->text, {symbol_kind::param, ty, param_idx++});
            while (lex.peek().kind == token_kind::comma) {
                lex.next();
                auto param = expect(ctx, lex, token_kind::id);
                auto ty = parse_type_annotation(ctx, lex);
                ctx.symtab.add(param->text, {symbol_kind::param, ty, param_idx++});
            }
        }
        expect(ctx, lex, token_kind::rpar);
        expect(ctx, lex, token_kind::colon);
        auto ty_annot = expect(ctx, lex, token_kind::id);
        auto ty = resolve_type(ty_annot->text);
        auto addr = ctx.asm64.size();

        ctx.frame.prologue(ctx.asm64);

        while (lex.peek().kind != token_kind::kw_end)
            parse_stmt(ctx, lex);

        ctx.symtab.exit();

        ctx.symtab.add(name->text, {symbol_kind::fun, ty, static_cast<int64_t>(addr)});

        expect(ctx, lex, token_kind::kw_end);
    }

    void parse(compiler_ctx &ctx, std::istream &in)
    {
        lexer lex{in};
        parse_function_def(ctx, lex);
    }
};

